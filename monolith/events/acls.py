from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json, requests

def get_photo(city, state):

    url = f"https://api.pexels.com/v1/search?query={city}{state}"
    headers = {"Authorization":PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    photo = json.loads(response.content)
    print(photo)
    if not photo["photos"]:
        return None
    picture = {
        "picture_url": (photo["photos"][0]["url"]),
    }

    return picture

def get_weather_data(city, state):
    latlonurl = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},us&limit=1&appid={OPEN_WEATHER_API_KEY}"
    latlonresponse = requests.get(latlonurl)
    lat_lon = latlonresponse.json()


    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat_lon[0]["lat"]}&lon={lat_lon[0]["lon"]}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    weatherdata = json.loads(response.content)

    print(weatherdata)
    weather = {
        "temperature": weatherdata["main"]["temp"],
        "description": weatherdata["weather"][0]["description"],
    }
    return weather
